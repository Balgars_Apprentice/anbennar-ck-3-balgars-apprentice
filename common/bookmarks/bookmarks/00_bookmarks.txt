﻿bm_2000_test = {
	start_date = 1022.1.1
	is_playable = yes
	group = bm_group_1

	character = {
		name = "bookmark_test_marion_silmuna"
		dynasty = dynasty_silmuna
		dynasty_splendor_level = 1
		type = male
		birth = 1001.2.19
		title = k_dameria
		government = feudal_government
		culture = damerian
		religion = cult_of_the_dame
		difficulty = "BOOKMARK_CHARACTER_DIFFICULTY_EASY"
		history_id = silmuna_0001
		position = { 900 530 }

		animation = disapproval
	}

	# character = {
		# name = "bookmark_test_maugun_of_ording"
		# history_id = 160
		# dynasty = dynasty_ording
		# dynasty_splendor_level = 1
		# type = male
		# birth = 981.04.24
		# title = d_great_ording
		# government = feudal_government
		# culture = lorenti
		# religion = court_of_adean
		# difficulty = "BOOKMARK_CHARACTER_DIFFICULTY_EASY"
		# position = { 400 530 }
	# }
	
	character = { # to test laamp
		name = "bookmark_test_naarwen_silnara"
		dynasty = dynasty_silnara
		dynasty_splendor_level = 1
		type = female
		birth = 897.7.21
		title = d_laamp_narawen
		government = landless_adventurer_government
		culture = moon_elvish
		religion = tefori_damish
		difficulty = "BOOKMARK_CHARACTER_DIFFICULTY_EASY"
		history_id = 16
		position = { 1400 530 }

		animation = disapproval
	}
}

