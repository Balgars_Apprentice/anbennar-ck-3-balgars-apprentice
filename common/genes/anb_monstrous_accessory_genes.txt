﻿

accessory_genes = {

	tails = {
		inheritable = yes
		no_tail = {
			index = 0
			male = {
				1 = empty
			}
			female = male
			boy = male
			girl = female
		}	

		kobold_tail = {
			index = 1
			male = {
				1 = kobold_tail_01
			}
			female = male
			boy = male
			girl = male
		}	
	}
				


	monstrous_head_attachment_01 = {
		inheritable = yes
		no_attachment_01 = {
			index = 0
			male = {
				1 = empty
			}
			female = male
			boy = male
			girl = female
		}	

		kobold_horns = {
			index = 1
			male = {
				1 = male_kobold_horns_straight
				1 = male_kobold_horns_curved
				1 = male_kobold_horns_goat
			}
			female = {
				1 = female_kobold_horns_straight
				1 = female_kobold_horns_curved
				1 = female_kobold_horns_goat
			}
			boy = male
			girl = female
		}		
	}

	monstrous_head_attachment_02 = {
		inheritable = yes
		no_attachment_02 = {
			index = 0
			male = {
				1 = empty
			}
			female = male
			boy = male
			girl = female
		}	

		kobold_jaw_spikes = {
			index = 1
			male = {
				1 = empty
				2 = male_kobold_jaw_spikes_A
				2 = male_kobold_jaw_spikes_B
				2 = male_kobold_jaw_spikes_C
				2 = male_kobold_jaw_spikes_D
				2 = male_kobold_jaw_spikes_E
				2 = male_kobold_jaw_spikes_F
				2 = male_kobold_jaw_spikes_G

			}
			female = {
				1 = empty
				2 = female_kobold_jaw_spikes_A
				2 = female_kobold_jaw_spikes_B
				2 = female_kobold_jaw_spikes_C
				2 = female_kobold_jaw_spikes_D
				2 = female_kobold_jaw_spikes_E
				2 = female_kobold_jaw_spikes_F
				2 = female_kobold_jaw_spikes_G

			}
			boy = male
			girl = female
		}
	}

	monstrous_head_attachment_03 = {
		inheritable = yes
		no_attachment_03 = {
			index = 0
			male = {
				1 = empty
			}
			female = male
			boy = male
			girl = female
		}	

		kobold_cheek_spikes = {
			index = 1
			male = {
				12 = empty
				1 = male_kobold_cheek_spikes_straight_A
				1 = male_kobold_cheek_spikes_curved_A
				1 = male_kobold_cheek_spikes_straight_B
				1 = male_kobold_cheek_spikes_curved_B
				1 = male_kobold_cheek_spikes_straight_C
				1 = male_kobold_cheek_spikes_curved_C
				1 = male_kobold_cheek_spikes_straight_D
				1 = male_kobold_cheek_spikes_curved_D
				1 = male_kobold_cheek_spikes_straight_E
				1 = male_kobold_cheek_spikes_curved_E
				1 = male_kobold_cheek_spikes_straight_F
				1 = male_kobold_cheek_spikes_curved_F


			}
			female = {
				28 = empty
				1 = female_kobold_cheek_spikes_straight_A
				1 = female_kobold_cheek_spikes_curved_A
				1 = female_kobold_cheek_spikes_straight_B
				1 = female_kobold_cheek_spikes_curved_B
				1 = female_kobold_cheek_spikes_straight_C
				1 = female_kobold_cheek_spikes_curved_C
				1 = female_kobold_cheek_spikes_straight_D
				1 = female_kobold_cheek_spikes_curved_D
				1 = female_kobold_cheek_spikes_straight_E
				1 = female_kobold_cheek_spikes_curved_E
				1 = female_kobold_cheek_spikes_straight_F
				1 = female_kobold_cheek_spikes_curved_F


			}
			boy = male
			girl = female
		}
	}

	monstrous_head_attachment_04 = {
		inheritable = yes
		no_attachment_04 = {
			index = 0
			male = {
				1 = empty
			}
			female = male
			boy = male
			girl = female
		}	

		kobold_eyebrow_spikes = {
			index = 1
			male = {
				14 = empty
				1 = male_kobold_eyebrow_spikes_straight_A
				1 = male_kobold_eyebrow_spikes_curved_A
				1 = male_kobold_eyebrow_spikes_straight_B
				1 = male_kobold_eyebrow_spikes_curved_B
				1 = male_kobold_eyebrow_spikes_straight_C
				1 = male_kobold_eyebrow_spikes_curved_C
				1 = male_kobold_eyebrow_spikes_straight_D
				1 = male_kobold_eyebrow_spikes_curved_D
				1 = male_kobold_eyebrow_spikes_straight_E
				1 = male_kobold_eyebrow_spikes_curved_E
				1 = male_kobold_eyebrow_spikes_straight_F
				1 = male_kobold_eyebrow_spikes_curved_F
				1 = male_kobold_eyebrow_spikes_straight_G
				1 = male_kobold_eyebrow_spikes_curved_G


			}
			female = {
				28 = empty
				1 = female_kobold_eyebrow_spikes_straight_A
				1 = female_kobold_eyebrow_spikes_curved_A
				1 = female_kobold_eyebrow_spikes_straight_B
				1 = female_kobold_eyebrow_spikes_curved_B
				1 = female_kobold_eyebrow_spikes_straight_C
				1 = female_kobold_eyebrow_spikes_curved_C
				1 = female_kobold_eyebrow_spikes_straight_D
				1 = female_kobold_eyebrow_spikes_curved_D
				1 = female_kobold_eyebrow_spikes_straight_E
				1 = female_kobold_eyebrow_spikes_curved_E
				1 = female_kobold_eyebrow_spikes_straight_F
				1 = female_kobold_eyebrow_spikes_curved_F
				1 = female_kobold_eyebrow_spikes_straight_G
				1 = female_kobold_eyebrow_spikes_curved_G


			}
			boy = male
			girl = female
		}
	}
}