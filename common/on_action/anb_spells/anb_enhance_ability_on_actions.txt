﻿###################################################
# EVENT ON_ACTIONS FOR ENHANCE ABILITY
###################################################

anb_enhance_ability_ongoing = {
	trigger = {
		exists = scope:scheme
	}

	random_events = {
		chance_to_happen = 50
		200 = 0
		#Enhance ability specific events
		100 = anb_enhance_ability_ongoing.1 #Target has high skill
		100 = anb_enhance_ability_ongoing.2 #Target has low skill
		100 = anb_enhance_ability_ongoing.3 #Target pushes you to try harder
		100 = anb_enhance_ability_ongoing.4 #Find a fitting material component

		#General spellcasting events
		50 = anb_spellcasting_general_ongoing_events.1 #Passage in an ancient lorebook
		50 = anb_spellcasting_general_ongoing_events.2 #Stress of casting magic leaves a toll on your health
		50 = anb_spellcasting_general_ongoing_events.3 #Target starts to have doubts
		50 = anb_spellcasting_general_ongoing_events.4 #Ingest Damestear potion
	}
}
