﻿dragon_cult_religion = {
	family = rf_dragon_cult
	graphical_faith = pagan_gfx
	doctrine = dragon_cult_hostility_doctrine 

	pagan_roots = yes

	#Main Group
	doctrine = doctrine_no_head
	doctrine = doctrine_gender_equal
	doctrine = doctrine_pluralism_righteous
	doctrine = doctrine_theocracy_temporal

	#Marriage
	doctrine = doctrine_concubines
	doctrine = doctrine_divorce_allowed
	doctrine = doctrine_bastardry_none
	doctrine = doctrine_consanguinity_cousins

	#Crimes
	doctrine = doctrine_homosexuality_shunned
	doctrine = doctrine_adultery_men_shunned
	doctrine = doctrine_adultery_women_shunned
	doctrine = doctrine_kinslaying_any_dynasty_member_crime
	doctrine = doctrine_deviancy_shunned
	doctrine = doctrine_witchcraft_accepted

	#Clerical Functions
	doctrine = doctrine_clerical_function_recruitment
	doctrine = doctrine_clerical_gender_either
	doctrine = doctrine_clerical_marriage_allowed
	doctrine = doctrine_clerical_succession_temporal_appointment
	
	#Allow pilgrimages
	doctrine = doctrine_pilgrimage_encouraged
	
	#Funeral tradition
	doctrine = doctrine_funeral_cremation

	traits = {
		virtues = { brave just honest }
		sins = { craven arbitrary deceitful }
	}

	custom_faith_icons = {
		custom_faith_1 custom_faith_2 custom_faith_3 custom_faith_4 custom_faith_5 custom_faith_6 custom_faith_7 custom_faith_8 custom_faith_9 custom_faith_10 dualism_custom_1 zoroastrian_custom_1 zoroastrian_custom_2 buddhism_custom_1 buddhism_custom_2 buddhism_custom_3 buddhism_custom_4 taoism_custom_1 yazidi_custom_1 sunni_custom_2 sunni_custom_3 sunni_custom_4 ibadi_custom muhakkima_1 muhakkima_2 muhakkima_4 muhakkima_5 muhakkima_6 judaism_custom_1
	}

	holy_order_names = {
		{ name = holy_order_guardians_of_divinerealm }
		{ name = holy_order_faithful_of_highgod }
		{ name = holy_order_warriors_of_the_symbol }
	}

	holy_order_maa = { horse_archers }

	localization = {
		HighGodName = dragon_cult_high_god_name
		HighGodName2 = dragon_cult_high_god_name_2
		HighGodNamePossessive = dragon_cult_high_god_name_possessive
		HighGodNameSheHe = CHARACTER_SHEHE_THEY
		HighGodHerselfHimself = CHARACTER_THEMSELVES
		HighGodHerHis = CHARACTER_HERHIS_THEIR
		HighGodNameAlternate = dragon_cult_high_god_name_alternate
		HighGodNameAlternatePossessive = dragon_cult_high_god_name_alternate_possessive

		#Creator
		CreatorName = dragon_cult_creator_god_name
		CreatorNamePossessive = dragon_cult_creator_god_name_possessive
		CreatorSheHe = CHARACTER_SHEHE_THEY
		CreatorHerHis = CHARACTER_HERHIS_THEIR
		CreatorHerHim = CHARACTER_HERHIM_THEM

		#HealthGod
		HealthGodName = dragon_cult_health_god_name
		HealthGodNamePossessive = dragon_cult_health_god_name_possessive
		HealthGodSheHe = CHARACTER_SHEHE_SHE
		HealthGodHerHis = CHARACTER_HERHIS_HER
		HealthGodHerHim = CHARACTER_HERHIM_HER
		
		#FertilityGod
		FertilityGodName = dragon_cult_fertility_god_name
		FertilityGodNamePossessive = dragon_cult_fertility_god_name_possessive
		FertilityGodSheHe = CHARACTER_SHEHE_THEY
		FertilityGodHerHis = CHARACTER_HERHIS_THEIR
		FertilityGodHerHim = CHARACTER_HERHIM_THEM

		#WealthGod
		WealthGodName = dragon_cult_wealth_god_name
		WealthGodNamePossessive = dragon_cult_wealth_god_name_possessive
		WealthGodSheHe = CHARACTER_SHEHE_HE
		WealthGodHerHis = CHARACTER_HERHIS_HIS
		WealthGodHerHim = CHARACTER_HERHIM_HIM

		#HouseholdGod
		HouseholdGodName = dragon_cult_household_god_name
		HouseholdGodNamePossessive = dragon_cult_household_god_name_possessive
		HouseholdGodSheHe = CHARACTER_SHEHE_THEY
		HouseholdGodHerHis = CHARACTER_HERHIS_THEIR
		HouseholdGodHerHim = CHARACTER_HERHIM_THEM

		#FateGod
		FateGodName = dragon_cult_fate_god_name
		FateGodNamePossessive = dragon_cult_fate_god_name_possessive
		FateGodSheHe = CHARACTER_SHEHE_HE
		FateGodHerHis = CHARACTER_HERHIS_HIS
		FateGodHerHim = CHARACTER_HERHIM_HIM

		#KnowledgeGod
		KnowledgeGodName = dragon_cult_knowledge_god_name
		KnowledgeGodNamePossessive = dragon_cult_knowledge_god_name_possessive
		KnowledgeGodSheHe = CHARACTER_SHEHE_HE
		KnowledgeGodHerHis = CHARACTER_HERHIS_HIS
		KnowledgeGodHerHim = CHARACTER_HERHIM_HIM

		#WarGod
		WarGodName = dragon_cult_war_god_name
		WarGodNamePossessive = dragon_cult_war_god_name_possessive
		WarGodSheHe = CHARACTER_SHEHE_HE
		WarGodHerHis = CHARACTER_HERHIS_HIS
		WarGodHerHim = CHARACTER_HERHIM_HIM

		#TricksterGod
		TricksterGodName = dragon_cult_trickster_god_name
		TricksterGodNamePossessive = dragon_cult_trickster_god_name_possessive
		TricksterGodSheHe = CHARACTER_SHEHE_HE
		TricksterGodHerHis = CHARACTER_HERHIS_HIS
		TricksterGodHerHim = CHARACTER_HERHIM_HIM

		#NightGod
		NightGodName = dragon_cult_night_god_name
		NightGodNamePossessive = dragon_cult_night_god_name_possessive
		NightGodSheHe = CHARACTER_SHEHE_SHE
		NightGodHerHis = CHARACTER_HERHIS_HER
		NightGodHerHim = CHARACTER_HERHIM_HER

		#WaterGod
		WaterGodName = dragon_cult_water_god_name
		WaterGodNamePossessive = dragon_cult_water_god_name_possessive
		WaterGodSheHe = CHARACTER_SHEHE_THEY
		WaterGodHerHis = CHARACTER_HERHIS_THEIR
		WaterGodHerHim = CHARACTER_HERHIM_THEM

		#DreamGod
		DreamGodName = dragon_cult_dream_god_name
		DreamGodNamePossessive = dragon_cult_dream_god_name_posessive
		DreamGodSheHe = CHARACTER_SHEHE_SHE
		DreamGodHerHis = CHARACTER_HERHIS_HER
		DreamGodHerHim = CHARACTER_HERHIM_HER

		#AuthorityGod
		AuthorityGodName = dragon_cult_authority_god_name
		AuthorityGodNamePossessive = dragon_cult_authority_god_name_possessive
		AuthorityGodSheHe = CHARACTER_SHEHE_HE
		AuthorityGodHerHis = CHARACTER_HERHIS_HIS
		AuthorityGodHerHim = CHARACTER_HERHIM_HIM

		PantheonTerm = dragon_cult_pantheon
		PantheonTerm2 = dragon_cult_pantheon
		PantheonTerm3 = dragon_cult_pantheon
		PantheonTermHasHave = pantheon_term_have
		GoodGodNames = { dragon_cult_high_god_name dragon_cult_high_god_name_alternate }
		DevilName = dragon_cult_devil_name #
		DevilNamePossessive = dragon_cult_devil_name_possessive #
		DevilSheHe = CHARACTER_SHEHE_IT #
		DevilHerHis = CHARACTER_HERHIS_ITS #
		DevilHerselfHimself = paganism_devil_herselfhimself #
		EvilGodNames = { paganism_devil_name paganism_evil_god_decay } #
		HouseOfWorship = dragon_cult_house_of_worship
		HouseOfWorship2 = dragon_cult_house_of_worship_2
		HouseOfWorship3 = dragon_cult_house_of_worship_3
		HouseOfWorshipPlural = dragon_cult_house_of_worship_plural
		ReligiousSymbol = dragon_cult_religious_symbol
		ReligiousSymbol2 = dragon_cult_religious_symbol_2 
		ReligiousSymbol3 = dragon_cult_religious_symbol_3
		ReligiousText = dragon_cult_religious_text
		ReligiousText2 = dragon_cult_religious_text_2
		ReligiousText3 = dragon_cult_religious_text_3
		ReligiousHeadName = dragon_cult_religious_head_title
		ReligiousHeadTitleName = dragon_cult_religious_head_title_name
		DevoteeMale = dragon_cult_devotee
		DevoteeMalePlural = dragon_cult_devoteeplural
		DevoteeFemale = dragon_cult_devotee
		DevoteeFemalePlural = dragon_cult_devoteeplural
		DevoteeNeuter = dragon_cult_devotee
		DevoteeNeuterPlural = dragon_cult_devoteeplural
		PriestMale = dragon_cult_priest
		PriestMalePlural = dragon_cult_priest_plural
		PriestFemale = dragon_cult_priest
		PriestFemalePlural = dragon_cult_priest_plural
		PriestNeuter = dragon_cult_priest
		PriestNeuterPlural = dragon_cult_priest_plural
		AltPriestTermPlural = dragon_cult_priest_alternate_plural
		BishopMale = dragon_cult_bishop
		BishopMalePlural = dragon_cult_bishop_plural
		BishopFemale = dragon_cult_bishop
		BishopFemalePlural = dragon_cult_bishop_plural
		BishopNeuter = dragon_cult_bishop
		BishopNeuterPlural = dragon_cult_bishop_plural
		DivineRealm = dragon_cult_divine_realm
		DivineRealm2 = dragon_cult_divine_realm_2
		DivineRealm3 = dragon_cult_divine_realm_3
		PositiveAfterLife = dragon_cult_good_afterlife
		PositiveAfterLife2 = dragon_cult_good_afterlife
		PositiveAfterLife3 = dragon_cult_good_afterlife
		NegativeAfterLife = dragon_cult_bad_afterlife
		NegativeAfterLife2 = dragon_cult_bad_afterlife
		NegativeAfterLife3 = dragon_cult_bad_afterlife
		DeathDeityName = dragon_cult_death_deity_name
		DeathDeityNamePossessive = dragon_cult_death_deity_name_possessive
		DeathDeitySheHe = dragon_cult_devil_shehe
		DeathDeityHerHis = dragon_cult_death_deity_herhis
		DeathDeityHerHim = CHARACTER_HERHIM_HIM
		
		GHWName = ghw_great_holy_war
		GHWNamePlural = ghw_great_holy_wars
	}	

	faiths = {
		korbarid_dragon_cult = { 
			color = { 156 183 179 }
			icon = the_dame
			religious_head = d_mirsalkomanta
			
			holy_site = karns_hold
			holy_site = north_citadel
			holy_site = banesfork
			holy_site = ravenhill
			holy_site = corvelds_coast

			doctrine = tenet_balakurbalan
			doctrine = tenet_struggle_submission
			doctrine = tenet_esotericism
			
			#Main Group
				doctrine = doctrine_spiritual_head
				doctrine = doctrine_gender_male_dominated
				doctrine = doctrine_pluralism_fundamentalist
			
			#Marriage
				doctrine = doctrine_divorce_approval
				doctrine = doctrine_bastardry_legitimization
				doctrine = doctrine_consanguinity_aunt_nephew_and_uncle_niece
				

			#Crimes
				doctrine = doctrine_homosexuality_accepted
				doctrine = doctrine_adultery_men_accepted
				doctrine = doctrine_adultery_women_shunned			
				doctrine = doctrine_witchcraft_crime
				
			#Clerical Functions
				doctrine = doctrine_clerical_gender_male_only				
				doctrine = doctrine_clerical_succession_spiritual_fixed_appointment
				
			#Funeral tradition
				doctrine = doctrine_funeral_sky_burial
			
			localization = {
			
				HighGodName = korbarid_balakururi
				HighGodName2 = korbarid_balakururi
				HighGodNamePossessive = korbarid_balakururi_possessive
				HighGodNameSheHe = CHARACTER_SHEHE_THEY
				HighGodHerselfHimself = CHARACTER_THEMSELVES
				HighGodHerHis = CHARACTER_HERHIS_THEIR
				HighGodNameAlternate = korbarid_balakururi_alternate
				HighGodNameAlternatePossessive = korbarid_balakururi_alternate_possessive

				#Creator
				CreatorName = korbarid_balakururiuri
				CreatorNamePossessive = korbarid_balakururi_possessive
				CreatorSheHe = CHARACTER_SHEHE_THEY
				CreatorHerHis = CHARACTER_HERHIS_THEIR
				CreatorHerHim = CHARACTER_HERHIM_THEM

				#HealthGod
				HealthGodName = korbarid_balakururi
				HealthGodNamePossessive = korbarid_balakururi_possessive
				HealthGodSheHe = CHARACTER_SHEHE_THEY
				HealthGodHerHis = CHARACTER_HERHIS_THEIR
				HealthGodHerHim = CHARACTER_HERHIM_THEM
				
				#FertilityGod
				FertilityGodName = korbarid_balakururi
				FertilityGodNamePossessive = korbarid_balakururi_possessive
				FertilityGodSheHe = CHARACTER_SHEHE_THEY
				FertilityGodHerHis = CHARACTER_HERHIS_THEIR
				FertilityGodHerHim = CHARACTER_HERHIM_THEM

				#WealthGod
				WealthGodName = korbarid_balakururi
				WealthGodNamePossessive = korbarid_balakururi_possessive
				WealthGodSheHe = CHARACTER_SHEHE_THEY
				WealthGodHerHis = CHARACTER_HERHIS_THEIR
				WealthGodHerHim = CHARACTER_HERHIM_THEM

				#HouseholdGod
				HouseholdGodName = korbarid_balakururi
				HouseholdGodNamePossessive = korbarid_balakururi_possessive
				HouseholdGodSheHe = CHARACTER_SHEHE_THEY
				HouseholdGodHerHis = CHARACTER_HERHIS_THEIR
				HouseholdGodHerHim = CHARACTER_HERHIM_THEM

				#FateGod
				FateGodName = korbarid_balakururi
				FateGodNamePossessive = korbarid_balakururi_possessive
				FateGodSheHe = CHARACTER_SHEHE_THEY
				FateGodHerHis = CHARACTER_HERHIS_THEIR
				FateGodHerHim = CHARACTER_HERHIM_THEM

				#KnowledgeGod
				KnowledgeGodName = korbarid_balakururi
				KnowledgeGodNamePossessive = korbarid_balakururi_possessive
				KnowledgeGodSheHe = CHARACTER_SHEHE_THEY
				KnowledgeGodHerHis = CHARACTER_HERHIS_THEIR
				KnowledgeGodHerHim = CHARACTER_HERHIM_THEM

				#WarGod
				WarGodName = korbarid_balakururi
				WarGodNamePossessive = korbarid_balakururi_possessive
				WarGodSheHe = CHARACTER_SHEHE_THEY
				WarGodHerHis = CHARACTER_HERHIS_THEIR
				WarGodHerHim = CHARACTER_HERHIM_THEM

				#TricksterGod
				TricksterGodName = korbarid_balakururi
				TricksterGodNamePossessive = korbarid_balakururi_possessive
				TricksterGodSheHe = CHARACTER_SHEHE_THEY
				TricksterGodHerHis = CHARACTER_HERHIS_THEIR
				TricksterGodHerHim = CHARACTER_HERHIM_THEM

				#NightGod
				NightGodName = korbarid_balakururi
				NightGodNamePossessive = korbarid_balakururi_possessive
				NightGodSheHe = CHARACTER_SHEHE_THEY
				NightGodHerHis = CHARACTER_HERHIS_THEIR
				NightGodHerHim = CHARACTER_HERHIM_THEM

				#WaterGod
				WaterGodName = korbarid_balakururi
				WaterGodNamePossessive = korbarid_balakururi_possessive
				WaterGodSheHe = CHARACTER_SHEHE_THEY
				WaterGodHerHis = CHARACTER_HERHIS_THEIR
				WaterGodHerHim = CHARACTER_HERHIM_THEM


				PantheonTerm = korbarid_balakururi
				PantheonTermHasHave = pantheon_term_have
				GoodGodNames = { korbarid_balakururi korbarid_garginbalakur korbarid_sekasbalakur } 
				DevilName = korbarid_agiuțări
				DevilNamePossessive = korbarid_agiuțări_possessive
				DevilSheHe = CHARACTER_SHEHE_THEY
				DevilHerHis = CHARACTER_HERHIS_THEIR
				DevilHerselfHimself = CHARACTER_THEMSELVES
				EvilGodNames = { korbarid_agiuțări }
				HouseOfWorship = korbarid_house_of_worship
				HouseOfWorshipPlural = korbarid_house_of_worship_plural
				ReligiousSymbol = korbarid_religious_symbol
				ReligiousText = korbarid_religious_text
				ReligiousHeadName = korbarid_religious_head_title
				ReligiousHeadTitleName = korbarid_religious_head_title_name
				DevoteeMale = korbarid_devotee
				DevoteeMalePlural = korbarid_devoteeplural
				DevoteeFemale = korbarid_devotee
				DevoteeFemalePlural = korbarid_devoteeplural
				DevoteeNeuter = korbarid_devotee
				DevoteeNeuterPlural = korbarid_devoteeplural
				PriestMale = korbarid_priest
				PriestMalePlural = korbarid_priest_plural
				PriestFemale = korbarid_priest
				PriestFemalePlural = korbarid_priest_plural
				PriestNeuter = korbarid_priest
				PriestNeuterPlural = korbarid_priest_plural
				AltPriestTermPlural = korbarid_priest_alternate_plural
				BishopMale = korbarid_priest
				BishopMalePlural = korbarid_priest_plural
				BishopFemale = korbarid_priest
				BishopFemalePlural = korbarid_priest_plural
				BishopNeuter = korbarid_priest
				BishopNeuterPlural = korbarid_priest_plural
				DivineRealm = korbarid_divine_realm
				PositiveAfterLife = korbarid_divine_realm
				NegativeAfterLife = korbarid_afterlife
				DeathDeityName = korbarid_death_name
				DeathDeityNamePossessive = korbarid_death_name_possessive
				DeathDeitySheHe = CHARACTER_SHEHE_IT
				DeathDeityHerHis = CHARACTER_HERHIS_ITS
				DeathDeityHerHim = CHARACTER_HERHIM_IT
				
				GHWName = korbarid_great_holy_war
				GHWNamePlural = korbarid_great_holy_wars
			}	
		}
		
		elkaesal_dragon_cult = {
			color = { 222 226 197 }
			icon = the_dame
			reformed_icon = the_dame

			holy_site = bal_vroren #Destroyed by Elkaesal
			holy_site = skaldol
			holy_site = algrar
			holy_site = jotunhamr
			#holy_site = elkaesals_slumber #underneath jarnklo harpies roost, currently offmap
			
			doctrine = unreformed_faith_doctrine
			
			doctrine = tenet_esotericism
			doctrine = tenet_struggle_submission
			doctrine = tenet_gruesome_festivals #TODO - replace with custom Waking the Dragon tenet
			
			doctrine = doctrine_kinslaying_accepted
			doctrine = doctrine_pluralism_fundamentalist
			doctrine = doctrine_theocracy_lay_clergy
			doctrine = doctrine_pilgrimage_mandatory
			doctrine = doctrine_clerical_function_taxation
			
			localization = {
				HighGodName = elkaesal_high_god_name #Elkaesal
				HighGodName2 = elkaesal_high_god_name
				HighGodNamePossessive = elkaesal_high_god_name_possessive
				HighGodNameSheHe = CHARACTER_SHEHE_SHE
				HighGodHerselfHimself = CHARACTER_HERSELF
				HighGodHerHis = CHARACTER_HERHIS_HER
				HighGodNameAlternate = elkaesal_high_god_name_alternate #The White Dragon
				HighGodNameAlternatePossessive = elkaesal_high_god_name_alternate_possessive
			}
			
		}
		
		nimrithani = {
			color = { 137 12 10 }
			icon = the_dame
			
			holy_site = khugdihr
			holy_site = damescrown
			holy_site = old_damenath
			holy_site = redfort
			#holy_site = nimriths_emergence #Located in Serpentspine
			
			doctrine = tenet_dragon_hoarding
			doctrine = tenet_false_conversion_sanction
			doctrine = tenet_pursuit_of_power
			
			doctrine = doctrine_consanguinity_aunt_nephew_and_uncle_niece
			doctrine = doctrine_clerical_function_taxation
			doctrine = doctrine_clerical_succession_temporal_fixed_appointment
			
			localization = {
				HighGodName = nimrithani_high_god_name #Nimrith
				HighGodName2 = nimrithani_high_god_name
				HighGodNamePossessive = nimrithani_high_god_name_possessive
				HighGodNameSheHe = CHARACTER_SHEHE_HE
				HighGodHerselfHimself = CHARACTER_HIMSELF
				HighGodHerHis = CHARACTER_HERHIS_HIS
				HighGodNameAlternate = nimrithani_high_god_name_alternate #The Red Dragon
				HighGodNameAlternatePossessive = nimrithani_high_god_name_alternate_possessive
			}
		}
		
		zaam = {
			color = { 12 12 12 }
			icon = the_dame
			reformed_icon = the_dame
			
			holy_site = old_damenath #Destroyed by Zaamalot
			holy_site = bal_mire #Destroyed by Zaamalot, later center of his cult
			holy_site = westgate #Tragedy of the West Gate?
			holy_site = dragonforge #Place of Awakening
			#somewhere dragon coast? #Location of battle between Zaamalot and Alos
			
			doctrine = unreformed_faith_doctrine
			
			doctrine = tenet_warmonger
			doctrine = tenet_gruesome_festivals
			doctrine = tenet_pursuit_of_power
			
			doctrine = doctrine_monogamy
			doctrine = doctrine_bastardry_legitimization
			doctrine = doctrine_deviancy_crime
			doctrine = doctrine_adultery_women_crime
			doctrine = doctrine_witchcraft_crime
			doctrine = doctrine_kinslaying_accepted
			doctrine = doctrine_gender_male_dominated
			
			localization = {
				HighGodName = zaam_high_god_name #Zaamalot
				HighGodName2 = zaam_high_god_name
				HighGodNamePossessive = zaam_high_god_name_possessive
				HighGodNameSheHe = CHARACTER_SHEHE_HE
				HighGodHerselfHimself = CHARACTER_HIMSELF
				HighGodHerHis = CHARACTER_HERHIS_HIS
				HighGodNameAlternate = zaam_high_god_name_alternate #The Black Dragon
				HighGodNameAlternatePossessive = zaam_high_god_name_alternate_possessive
			}
		}
		
		kobold_dragon_cult = {
			color = { 3 85 124 }
			icon = kobold_dragon_cult
			reformed_icon = kobold_dragon_cult_reformed

			holy_site = soxun_kobildzex
			holy_site = fetaginbaia
			holy_site = mawdock
			holy_site = ayddexahea
			
			doctrine = unreformed_faith_doctrine
			
			doctrine =  tenet_dragon_hoarding
			doctrine =  tenet_communal_identity
			doctrine =  tenet_pursuit_of_power
			
			doctrine = doctrine_consanguinity_aunt_nephew_and_uncle_niece
			doctrine = doctrine_pluralism_fundamentalist
			doctrine = doctrine_clerical_succession_temporal_fixed_appointment
			
			localization = {
				PantheonTerm = kobold_dragon_cult_dragons
				PantheonTermHasHave = pantheon_term_have
				GoodGodNames = { kobold_dragon_cult_good_god_name kobold_dragon_cult_good_god_name_2 kobold_dragon_cult_good_god_name_3 }
				DevilName = dragon_cult_evil_god_name
				DevilNamePossessive = dragon_cult_evil_god_name_possessive
				DevilSheHe = CHARACTER_SHEHE_THEY
				DevilHerHis = CHARACTER_HERHIS_THEIR
				DevilHerselfHimself = CHARACTER_THEMSELVES
				EvilGodNames = { dragon_cult_evil_god_name }
				HighGodName = kobold_dragon_cult_high_god_name #Malliath
				HighGodName2 = kobold_dragon_cult_high_god_name
				HighGodNamePossessive = kobold_dragon_cult_high_god_name_possessive
				HighGodNameSheHe = CHARACTER_SHEHE_HE
				HighGodHerselfHimself = CHARACTER_HIMSELF
				HighGodHerHis = CHARACTER_HERHIS_HIS
				HighGodNameAlternate = kobold_dragon_cult_high_god_name_alternate #The Father of Dragons
				HighGodNameAlternatePossessive = kobold_high_god_name_alternate_possessive
				HouseOfWorship = kobold_dragon_cult_house_of_worship
				HouseOfWorshipPlural = kobold_dragon_cult_house_of_worship_plural
				ReligiousHeadName = kobold_dragon_cult_religious_head
				ReligiousHeadTitleName = kobold_dragon_cult_religious_head_title_name
				ReligiousSymbol = kobold_dragon_cult_religious_symbol
				ReligiousText = kobold_dragon_cult_religious_text
				DivineRealm = kobold_dragon_cult_divine_realm
				PositiveAfterLife = kobold_dragon_cult_good_afterlife
				NegativeAfterLife = kobold_dragon_cult_bad_afterlife
			}
		}
	}
}