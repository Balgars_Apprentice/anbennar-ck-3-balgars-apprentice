﻿marble_dwarf_minority_character = {
	age = { 40 120 }
	faith = this.faith
	culture = culture:marble_dwarvish
	gender_female_chance = 40
	trait = loyal
	#Education
	random_traits_list = {
		count = 1
		education_martial_3 = {}
		education_martial_4 = {}
		education_stewardship_3 = {}
		education_stewardship_4 = {}
		education_diplomacy_3 = {}
		education_diplomacy_4 = {}
		education_learning_3 = {}
		education_learning_4 = {}
	}
	#Personality
	random_traits_list = {
		count = 3
		brave = {}
		calm = {}
		wrathful = {}
		lustful = {}
		ambitious = {}
		content = {}
		diligent = {}
		vengeful = {}
		greedy = {}
		gregarious = {}
		honest = {}
		just = {}
		patient = {}
		impatient = {}
		stubborn = {}
	}
	#flavor traits
	random_traits_list = {
		count = 1
		stong = {}
		shrewd = {}
		drunkard = {}
		adventurer_follower = {}
		adventurer = {}
		architect = {}
		family_first = {}
		lifestyle_blademaster = { trigger = { gender = male } }
		lifestyle_physician = {}
		lifestyle_reveler = {}
	}
	#flavor genetic traits
	random_traits_list = {
		count = 1
		intellect_good_1 = {}
		beauty_good_1 = {}
		magical_affinity_1 = {}
		physique_good_1 = {}
		physique_good_2 = {}
		fecund = {}
	}
	#Anbennar
	after_creation = {
		if = {
			limit = {
				NOT = { has_racial_trait = yes }
			}
			assign_racial_trait_effect = yes
		}
		anb_generate_planetouched = yes
		anb_assign_religious_flavor_traits = yes
		set_character_flag = anb_do_not_go_wandering_flag
	}
}
ruby_dwarf_minority_character = {
	age = { 40 120 }
	faith = this.faith
	culture = culture:ruby_dwarvish
	gender_female_chance = 40
	trait = loyal
	#Education
	random_traits_list = {
		count = 1
		education_martial_3 = {}
		education_martial_4 = {}
		education_stewardship_3 = {}
		education_stewardship_4 = {}
		education_diplomacy_3 = {}
		education_diplomacy_4 = {}
		education_learning_3 = {}
		education_learning_4 = {}
	}
	#Personality
	random_traits_list = {
		count = 3
		brave = {}
		calm = {}
		wrathful = {}
		lustful = {}
		ambitious = {}
		content = {}
		diligent = {}
		vengeful = {}
		greedy = {}
		gregarious = {}
		honest = {}
		just = {}
		patient = {}
		impatient = {}
		stubborn = {}
	}
	#flavor traits
	random_traits_list = {
		count = 1
		stong = {}
		shrewd = {}
		drunkard = {}
		adventurer_follower = {}
		adventurer = {}
		architect = {}
		family_first = {}
		lifestyle_blademaster = { trigger = { gender = male } }
		lifestyle_physician = {}
		lifestyle_reveler = {}
	}
	#flavor genetic traits
	random_traits_list = {
		count = 1
		intellect_good_1 = {}
		beauty_good_1 = {}
		magical_affinity_1 = {}
		physique_good_1 = {}
		physique_good_2 = {}
		fecund = {}
	}
	#Anbennar
	after_creation = {
		if = {
			limit = {
				NOT = { has_racial_trait = yes }
			}
			assign_racial_trait_effect = yes
		}
		anb_generate_planetouched = yes
		anb_assign_religious_flavor_traits = yes
		set_character_flag = anb_do_not_go_wandering_flag
	}
}
moon_elf_minority_character = {
	age = { 80 150 }
	faith = this.faith
	culture = culture:moon_elvish
	gender_female_chance = 60
	trait = loyal
	#Education
	random_traits_list = {
		count = 1
		education_martial_3 = {}
		education_martial_4 = {}
		education_stewardship_3 = {}
		education_stewardship_4 = {}
		education_diplomacy_3 = {}
		education_diplomacy_4 = {}
		education_learning_3 = {}
		education_learning_4 = {}
		education_intrigue_3 = {}
		education_intrigue_4 = {}
	}
	#Personality
	random_traits_list = {
		count = 3
		brave = {}
		craven = {}
		calm = {}
		lustful = {}
		ambitious = {}
		content = {}
		diligent = {}
		forgiving = {}
		generous = {}
		gregarious = {}
		shy = {}
		honest = {}
		deceitful = {}
		just = {}
		patient = {}
		zealot = {}
		compassionate = {}
	}
	#flavor traits
	random_traits_list = {
		count = 1
		stong = {}
		shrewd = {}
		inappetetic = {}
		adventurer_follower = {}
		adventurer = {}
		strategist = {}
		diplomat = {}
		lifestyle_blademaster = {}
		lifestyle_physician = {}
		lifestyle_reveler = {}
		lifestyle_mystic = {}
	}
	#flavor genetic traits
	random_traits_list = {
		count = { 1 2 }
		intellect_good_1 = {}
		beauty_good_1 = {}
		beauty_good_2 = {}
		beauty_good_3 = {}
		magical_affinity_1 = {}
		magical_affinity_2 = {}
		magical_affinity_3 = {}
		physique_good_1 = {}
		fecund = {}
	}
	#Anbennar
	after_creation = {
		if = {
			limit = {
				NOT = { has_racial_trait = yes }
			}
			assign_racial_trait_effect = yes
		}
		anb_generate_planetouched = yes
		anb_assign_religious_flavor_traits = yes
		set_character_flag = anb_do_not_go_wandering_flag
	}
}