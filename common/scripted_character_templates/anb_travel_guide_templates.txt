﻿
caver_guide_dwarf = {
	age = { 30 150 }
	dynasty = none
	
	faith = root.faith # temporary, is defined later
	
	random_culture = { # make sure every place is covered
		culture:agate_dwarvish = {
			trigger = {
				root.capital_province = {
					geographical_region = world_cannor_east_castanor
				}
			}
		}
		culture:stone_dwarvish = {
			trigger = {
				root.capital_province = {
					OR = {
						geographical_region = world_cannor_east						
						geographical_region = world_cannor_west_alenor
						geographical_region = world_cannor_gerudia
					}
				}
			}
		}
		culture:ruby_dwarvish = {
			trigger = {
				root.capital_province = {
					OR = {
						geographical_region = world_cannor_west_lencenor
						geographical_region = world_cannor_west_dragon_coast						
						geographical_region = world_cannor_west_alenor
						geographical_region = world_cannor_gerudia
					}
				}
			}
		}
		culture:silver_dwarvish = {
			trigger = {
				root.capital_province = {
					OR = {
						geographical_region = world_cannor_west_dameshead						
						geographical_region = world_cannor_west_alenor
						geographical_region = world_cannor_gerudia
					}
				}
			}
		}
		culture:copper_dwarvish = {
			trigger = {
				root.capital_province = {
					geographical_region = world_sarhal
				}
			}
		}
		culture:citrine_dwarvish = {
			trigger = {
				root.capital_province = {
					geographical_region = world_sarhal_bulwar_central
				}
			}
		}
		culture:pyrite_dwarvish = {
			trigger = {
				root.capital_province = {
					geographical_region = world_sarhal_bulwar_central
				}
			}
		}
		culture:titanium_dwarvish = {
			trigger = {
				root.capital_province = {
					geographical_region = world_sarhal_bulwar_central
				}
			}
		}
		culture:gold_dwarvish = {
			trigger = {
				root.capital_province = {
					geographical_region = world_sarhal_bulwar_east
				}
			}
		}
		culture:topaz_dwarvish = {
			trigger = {
				root.capital_province = {
					geographical_region = world_sarhal_bulwar_east
				}
			}
		}
	}

	random_traits_list = {
		count = 1
		education_martial_1 = {}
		education_martial_2 = {}
		education_martial_3 = {}
		education_martial_4 = {}
	}
	trait = deep_terrain_expert
	random_traits = yes
	gender_female_chance = root_soldier_female_chance
	after_creation = {
		if = {
			limit = { NOT = { has_racial_trait = yes } }
			assign_racial_trait_effect = yes
		}
		random_list = {
			55 = { }
			45 = { add_trait = adventurer }
		}
		
		if = {
			limit = {
				OR = {
					culture = culture:silver_dwarvish
					culture = culture:stone_dwarvish
				}
			}
			set_character_faith = faith:cult_of_balgar
		}
		else_if = {
			limit = {
				OR = {
					culture = culture:ruby_dwarvish
					culture = culture:titanium_dwarvish #basing this off vicky 3
				}
			}
			set_character_faith = faith:dwarven_pantheon
		}
		else_if = {
			limit = {
				OR = {
					culture = culture:copper_dwarvish
					culture = culture:gold_dwarvish
					culture = culture:topaz_dwarvish
					culture = culture:citrine_dwarvish
					culture = culture:pyrite_dwarvish
					culture = culture:agate_dwarvish
				}
			}
			set_character_faith = faith:ancestor_worship
		}
	}
}

caver_guide_kobold = {
	age = { 30 55 }
	dynasty = none																																	
	
	faith = root.faith # temporary, is defined later
	
	random_culture = {
		culture:redscale = {
			trigger = {
				root.capital_province = {
					geographical_region = world_cannor_west
				}
			}
		}
		culture:bluescale = {
			trigger = {
				root.capital_province = {
					geographical_region = world_cannor_west
				}
			}
		}
		culture:greenscale = {
			trigger = {
				root.capital_province = {
					geographical_region = world_cannor_west
				}
			}
		}
		culture:darkscale = {
			trigger = {
				root.capital_province = {
					OR = {
						geographical_region = world_cannor_east
						geographical_region = world_sarhal
					}
				}
			}
		}
	}

	random_traits_list = {
		count = 1
		education_martial_1 = {}
		education_martial_2 = {}
		education_martial_3 = {}
		education_martial_4 = {}
	}
	trait = deep_terrain_expert
	random_traits = yes
	gender_female_chance = root_soldier_female_chance
	after_creation = {
		if = {
			limit = { NOT = { has_racial_trait = yes } }
			assign_racial_trait_effect = yes
		}
		random_list = {
			55 = { }
			45 = { add_trait = adventurer }
		}
		
		if = {
			limit = {
				culture = culture:darkscale
			}
			set_character_faith = faith:nimrithani
		}
		else_if = {
			limit = {
				OR = {
					culture = culture:bluescale
					culture = culture:greenscale
					culture = culture:redscale
				}
			}
			set_character_faith = faith:kobold_dragon_cult
		}
	}
}

# caver_guide_goblin = { # Anbennar TODO: implement when goblins are in
	
# }
