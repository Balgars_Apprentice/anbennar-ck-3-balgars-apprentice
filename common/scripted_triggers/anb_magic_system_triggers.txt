﻿
has_magical_affinity = {
	OR = {
		has_trait = magical_affinity_1
		has_trait = magical_affinity_2
		has_trait = magical_affinity_3
	}
}