﻿#Anbennar - magical affinity

#Anbennar
has_magical_affinity_trigger = {
	has_trait = magical_affinity
}

has_skaldic_tale_active = {
	OR = {
		has_character_modifier = skaldhyrric_dirge
		has_character_modifier = skaldhyrric_gjalund
		has_character_modifier = skaldhyrric_beralic
		has_character_modifier = skaldhyrric_voyage
		has_character_modifier = skaldhyrric_golden_forest
		has_character_modifier = skaldhyrric_master_boatbuilders
		has_character_modifier = skaldhyrric_dragon_and_skald
		has_character_modifier = skaldhyrric_old_winter_lullaby
	}
}

can_move_title_into_gerudia_trigger = {
	trigger_if = {
		limit = {
			title_is_valid_to_move_to_gerudia = { TITLE = $TITLE$ }
		}
		completely_controls = $TITLE$
	}
	trigger_else = {
		# TODO - Add a tooltip that says "has not moved title to Gerudia"
		always = no
	}
}

title_is_valid_to_move_to_gerudia = {
	title_has_de_jure = { TITLE = $TITLE$ }
	$TITLE$ = {
		NOT = { empire = title:e_gerudia } 
		NOT = { has_variable = kingdom_moved_into_gerudia_empire }
	}
}

title_has_de_jure = {
	$TITLE$ = { any_in_de_jure_hierarchy = { tier = tier_county } }
}

anb_ai_wants_to_promote_culture = { # Anbennar TODO: keep this up-to-date
	OR = {
		AND = { # Redfoot and Bluefoot want to become the only haflings (but leave viswallers alone)
			OR = {
				scope:councillor_liege.culture = culture:bluefoot_halfling
				scope:councillor_liege.culture = culture:redfoot_halfling
			}
			OR = {
				scope:county.culture = culture:ciderfoot_halfling
				scope:county.culture = culture:roysfoot_halfling
				scope:county.culture = culture:oakfoot_halfling
				scope:county.culture = culture:beefoot_halfling
				scope:county.culture = culture:hillfoot_halfling
			}
		}
		# Elvenized Cultures
		AND = { # Lorentish want to convert Lorenti
			scope:councillor_liege.culture = culture:lorentish
			OR = {
				scope:county.culture = culture:lorenti
			}
		}
		AND = { # Damerian want to convert Old Damerian and Lenco-Damerian
			scope:councillor_liege.culture = culture:damerian
			OR = {
				scope:county.culture = culture:old_damerian
				scope:county.culture = culture:lenco_damerian
			}
		}
		AND = { # Esmari want to convert Old Esmari
			scope:councillor_liege.culture = culture:esmari
			OR = {
				scope:county.culture = culture:old_esmari
			}
		}
		AND = { # Sorncosti want to convert Sormanni
			scope:councillor_liege.culture = culture:sorncosti
			OR = {
				scope:county.culture = culture:sormanni
			}
		}
		AND = { # Vernman want to convert Vernid
			scope:councillor_liege.culture = culture:vernman
			OR = {
				scope:county.culture = culture:vernid
			}
		}
		AND = { # Castellyrian want to convert Castanorian
			scope:councillor_liege.culture = culture:castellyrian
			OR = {
				scope:county.culture = culture:castanorian
				scope:county.culture = culture:black_castanorian
			}
		}
		# Newer cultures want to convert old cultures
		AND = { # The Corvurians want to convert Kobarid
			scope:councillor_liege.culture = culture:corvurian
			OR = {
				scope:county.culture = culture:korbarid
			}
		}
		AND = { # Gawedi want to convert Old Alenic
			scope:councillor_liege.culture = culture:gawedi
			OR = {
				scope:county.culture = culture:old_alenic
			}
		}
		AND = { # Arannese want to convert Milcori and Roilsardi
			scope:councillor_liege.culture = culture:arannese
			OR = {
				scope:county.culture = culture:milcori
				scope:county.culture = culture:roilsardi
			}
		}
	}
}

anb_ai_wants_to_convert_province = {
	trigger_if = { # Elven Forebears converts only elven provinces
		limit = { scope:councillor.faith = faith:elven_forebears }
		scope:county = { is_elvish_culture = yes }
	}
	trigger_else = {
		always = yes
	}
}

anb_has_patron_god_modifier_trigger = {
	OR = {
		# Regent Court
		has_character_modifier = patron_the_dame_modifier
		has_character_modifier = patron_munas_modifier
		has_character_modifier = patron_castellos_modifier
		has_character_modifier = patron_esmaryal_modifier
		has_character_modifier = patron_falah_modifier
		has_character_modifier = patron_nerat_modifier
		has_character_modifier = patron_adean_modifier
		has_character_modifier = patron_ryala_modifier
		has_character_modifier = patron_balgar_modifier
		has_character_modifier = patron_ara_modifier
		has_character_modifier = patron_minara_modifier
		has_character_modifier = patron_munas_modifier
		has_character_modifier = patron_nathalyne_modifier
		has_character_modifier = patron_begga_modifier
		has_character_modifier = patron_uelos_modifier
		has_character_modifier = patron_wip_modifier
		# Alenic
		has_character_modifier = patron_ariadas_modifier
		has_character_modifier = patron_histra_modifier
		has_character_modifier = patron_wulkas_modifier
		has_character_modifier = patron_ragna_modifier
		has_character_modifier = patron_treyana_modifier
		has_character_modifier = patron_hyntran_modifier
		has_character_modifier = patron_welas_modifier
		has_character_modifier = patron_rendan_modifier
		# Lencori
		has_character_modifier = patron_artanos_modifier
		has_character_modifier = patron_merisse_modifier
		has_character_modifier = patron_trovecos_modifier
		has_character_modifier = patron_careslobos_modifier
		has_character_modifier = patron_asmirethin_modifier
		has_character_modifier = patron_damarta_modifier
		has_character_modifier = patron_belouina_modifier
		has_character_modifier = patron_dercanos_modifier
		has_character_modifier = patron_turanos_modifier
		has_character_modifier = patron_sorbodua_modifier
		# Emergent Patrons
		has_character_modifier = anb_martial_patron_apot_modifier
		has_character_modifier = anb_diplomacy_patron_apot_modifier
		has_character_modifier = anb_stewardship_patron_apot_modifier
		has_character_modifier = anb_intrigue_patron_apot_modifier
		has_character_modifier = anb_learning_patron_apot_modifier
	}
}

anb_faith_has_patron_gods_trigger = {
	OR = {
		faith = faith:castanorian_pantheon
		faith = faith:regent_court
		faith = faith:cult_of_the_dame
		faith = faith:small_temple
		faith = faith:gawedi_temple
		faith = faith:whiterose_court
		faith = faith:cult_of_esmaryal 
		faith = faith:border_cults
		faith = faith:cult_of_falah
		faith = faith:cult_of_ara
		faith = faith:luna_damish
		faith = faith:adenican_adean
		faith = faith:cult_of_ryala
		faith = faith:court_of_adean
		faith = faith:southern_cult_of_castellos
		faith = faith:west_damish
		faith = faith:vernid_adean
		faith = faith:tefori_damish
		faith = faith:house_of_minara
		faith = faith:iochand_cult
		faith = faith:cult_of_the_lightfather
		faith = faith:reverian_cult
		faith = faith:cult_of_balgar
		faith = faith:cult_of_uelos
		faith = faith:moors_cult
		faith = faith:ansuwir
		faith = faith:eidoueni
	}
}

anb_has_custom_patron_gods_trigger = {
	anb_faith_has_patron_gods_trigger = no
	faith = {
		has_variable_list = patron_gods
	}
}

#minority system
anb_is_local_minority = {
	OR = {
		AND = {
			culture = culture:marble_dwarvish
			exists = scope:base.primary_title.title_province
			scope:base.primary_title.title_province = { has_province_modifier = marble_dwarvish_city }
		}
		AND = {
			culture = culture:ruby_dwarvish
			exists = scope:base.primary_title.title_province
			scope:base.primary_title.title_province = { has_province_modifier = ruby_dwarvish_city }
		}
		AND = {
			culture = culture:moon_elvish
			exists = scope:base.primary_title.title_province
			scope:base.primary_title.title_province = { has_province_modifier = moon_elvish_city }
		}
	}
}
anb_owner_of_minority = {
	OR = {
		AND = {
			culture = culture:marble_dwarvish
			capital_province = { has_province_modifier = marble_dwarvish_city }
		}
		AND = {
			culture = culture:ruby_dwarvish
			capital_province = { has_province_modifier = ruby_dwarvish_city }
		}
		AND = {
			culture = culture:moon_elvish
			capital_province = { has_province_modifier = moon_elvish_city }
		}
	}
}
anb_has_minority_modifier = {
	OR = {
		has_province_modifier = marble_dwarvish_city
		has_province_modifier = ruby_dwarvish_city
		has_province_modifier = moon_elvish_city
	}
}
has_planetouched_trait = {
	OR = {
		has_trait = planetouched_fire_1
		has_trait = planetouched_fire_2
		has_trait = planetouched_fire_3
		has_trait = planetouched_water_1
		has_trait = planetouched_water_2
		has_trait = planetouched_water_3
		has_trait = planetouched_earth_1
		has_trait = planetouched_earth_2
		has_trait = planetouched_earth_3
		has_trait = planetouched_wind_1
		has_trait = planetouched_wind_2
		has_trait = planetouched_wind_3
		has_trait = planetouched_life_1
		has_trait = planetouched_life_2
		has_trait = planetouched_life_3
		has_trait = planetouched_shadow_1
		has_trait = planetouched_shadow_2
		has_trait = planetouched_shadow_3
	}
}