﻿# Arda of Nurael
40000 = {
	name = "Arda"
	#dna = 40000_duke_arda
	dynasty = dynasty_nurael
	religion = elven_forebears
	culture = moon_elvish
	female = yes
	
	trait = race_elf
	trait = education_learning_2
	trait = content
	trait = compassionate
	trait = diligent
	trait = lifestyle_physician
	trait = elven_purist
	
	814.4.18 = {
		birth = yes
	}
	
	910.1.1 = {
		effect = {
			set_relation_friend = character:75	#Ibenion
			
			add_trait_xp = {
				trait = lifestyle_physician
				value = 100
			}
		}
	}
}

# Ultarion of Larthan
40001 = {
	name = "Ultarion"
	#dna = 40001_duke_ultarion
	dynasty = dynasty_larthan
	religion = elven_forebears
	culture = moon_elvish
	
	trait = race_elf
	trait = education_diplomacy_3
	trait = greedy
	trait = fickle
	trait = temperate
	
	735.12.1 = {
		birth = yes
	}
	
	910.1.1 = {
		effect = {
			set_relation_friend = character:75	#Ibenion
		}
	}
}

# Carodir of Larthan
40003 = {
	name = "Carodir"
	#dna = 40003_carodir
	dynasty = dynasty_silcarod
	religion = elven_forebears
	culture = moon_elvish
	
	trait = race_elf
	trait = education_martial_2
	trait = stubborn
	trait = wrathful
	trait = zealous
	
	785.3.3 = {
		birth = yes
	}
}

#Camnaril of Venail
40004 = {
	name = "Camnaril"
	dynasty = dynasty_truesight
	religion = elven_forebears
	culture = moon_elvish
	
	trait = race_elf
	trait = education_diplomacy_2
	trait = shy
	trait = greedy
	trait = honest
	
	837.7.13 = {
		birth = yes
		effect = {
			set_variable = no_purist_trait
		}
	}
	
	1001.1.1 = {
		effect = {
			set_relation_friend = character:40005	#Liandel
		}
	}
}

#Liandel of Edilliande
40005 = {
	name = "Liandel"
	dynasty = dynasty_seawatcher
	religion = elven_forebears
	culture = moon_elvish
	female = yes
	
	trait = race_elf
	trait = education_intrigue_3
	trait = deceitful
	trait = ambitious
	trait = zealous
	trait = beauty_good_2
	
	880.6.21 = {
		birth = yes
	}
	
	1004.1.1 = {
		add_matrilineal_spouse = 40006 #Finorian
	}
	
	1021.10.31 = {
		effect = {
			add_hook_no_toast = {
				type = favor_hook
				target = character:40004 #Camnaril
			}
		}
	}
}

# Spouse of Liandel
40006 = {
	name = "Finorian"
	religion = elven_forebears
	culture = moon_elvish
	
	trait = race_elf
	trait = education_stewardship_1
	trait = trusting
	trait = lustful
	trait = content
	
	848.3.18 = {
		birth = yes
	}
	
	1004.1.1 = {
		add_matrilineal_spouse = 40005	#Liandel
	}
}

# Spouse of the Count of Ilvandet
woodslover0001 = {
	name = "Imariel"
	dynasty = dynasty_woodslover
	religion = elven_forebears
	culture = moon_elvish
	female = yes
	father = woodslover0003
	
	trait = race_elf
	trait = education_martial_2
	trait = brave
	trait = impatient
	trait = forgiving
	trait = lifestyle_gardener
	trait = magical_affinity_2
	
	790.9.16 = {
		birth = yes
	}
	
	1012.5.9 = {
		add_spouse = ilvan_0001 #Lucius
	}
}

woodslover0002 = { #Brother
	name = "Thalanor"
	dynasty = dynasty_woodslover
	religion = elven_forebears
	culture = moon_elvish
	father = woodslover0003
	
	trait = race_elf
	trait = education_diplomacy_2
	trait = diligent
	trait = compassionate
	trait = arrogant
	trait = lifestyle_traveler
	trait = magical_affinity_2
	
	770.4.3 = {
		birth = yes
	}
	
	1001.3.14 = {
		death = {
			death_reason = death_killed_war_of_sorcerer_king
		}
	}
}

woodslover0003 = { #Dad
	name = "Threthinor"
	dynasty = dynasty_woodslover
	religion = elven_forebears
	culture = moon_elvish
	
	trait = race_elf
	trait = education_diplomacy_3
	trait = calm
	trait = impatient
	trait = magical_affinity_2
	trait = lifestyle_traveler
	
	512.6.3 = {
		birth = yes
	}
	
	894.11.4 = {
		death = yes
	}
}

leafdancer_0001 = { #father of Oloris
	name = "Varamael"
	religion = elven_forebears
	culture = moon_elvish
	
	trait = race_elf
	trait = education_learning_2
	trait = cynical
	trait = stubborn
	trait = honest
	
	616.11.9 = {
		birth = yes
	}
	
	788.1.1 = {
		add_spouse = leafdancer_0002
	}

	987.7.26 = {
		death = yes
	}
}

leafdancer_0002 = { #mother of Oloris
	name = "Erelassa"
	religion = elven_forebears
	culture = moon_elvish
	female = yes
	
	trait = race_elf
	trait = education_stewardship_2
	trait = chaste
	trait = craven
	trait = humble
	
	611.2.6 = {
		birth = yes
	}

	788.1.1 = {
		add_spouse = leafdancer_0001
	}
	
	979.4.25 = {
		death = yes
	}
}

leafdancer_0003 = { #Oloris
	name = "Oloris"
	dynasty = dynasty_leafdancer
	religion = cult_of_the_dame
	culture = moon_elvish
	father = leafdancer_0001
	mother = leafdancer_0002
	
	trait = race_elf
	trait = education_stewardship_3
	trait = diligent
	trait = patient
	trait = stubborn
	trait = lifestyle_gardener
	
	789.1.19 = {
		birth = yes
	}
	
	901.1.1 = {
		add_spouse = leafdancer_0004
	}
}

leafdancer_0004 = { #wife of Oloris
	name = "Selussa"
	religion = cult_of_the_dame
	culture = moon_elvish
	female = yes
	
	trait = race_elf
	trait = education_diplomacy_1
	trait = lustful
	trait = gluttonous
	trait = patient
	trait = lifestyle_traveler
	
	790.3.29 = {
		birth = yes
	}

	901.1.1 = {
		add_spouse = leafdancer_0003
	}
}

leafdancer_0005 = { #daughter of Oloris
	name = "Galindel"
	dynasty = dynasty_leafdancer
	religion = cult_of_the_dame
	culture = moon_elvish
	father = leafdancer_0003
	mother = leafdancer_0004
	female = yes
	
	trait = race_elf
	trait = education_stewardship_1
	trait = forgiving
	trait = patient
	trait = cynical
	
	902.9.15 = {
		birth = yes
	}
}
