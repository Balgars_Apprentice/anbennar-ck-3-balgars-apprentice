#k_heunthume
##d_heunthume
###c_heunthume
660 = {		#Heunthume

    # Misc
    culture = heunthulyra
    religion = hunt_of_the_myna
	holding = tribal_holding

    # History
}

2975 = {

    # Misc
	culture = heunthulyra
    religion = hunt_of_the_myna
	holding = none

    # History
}

2976 = {

    # Misc
	culture = heunthulyra
    religion = hunt_of_the_myna
	holding = church_holding

    # History
}

2977 = {

    # Misc
    culture = heunthulyra
    religion = hunt_of_the_myna
	holding = city_holding

    # History
}

2978 = {

    # Misc
    culture = heunthulyra
    religion = hunt_of_the_myna
	holding = none

    # History
}

###c_lisinyalen
663 = {		#Lisinyalen

    # Misc
    culture = heunthulyra
    religion = hunt_of_the_myna
	holding = tribal_holding

    # History
}

2979 = {

    # Misc
	culture = heunthulyra
    religion = hunt_of_the_myna
	holding = none

    # History
}

2980 = {

    # Misc
	culture = heunthulyra
    religion = hunt_of_the_myna
	holding = none

    # History
}

2981 = {

    # Misc
	culture = heunthulyra
    religion = hunt_of_the_myna
	holding = none

    # History
}

###c_hytiranyalen
661 = {		#Hytiranyalen

    # Misc
    culture = heunthulyra
    religion = hunt_of_the_myna
	holding = tribal_holding

    # History
}

2982 = {

    # Misc
	culture = heunthulyra
    religion = hunt_of_the_myna
	holding = none

    # History
}

2983 = {

    # Misc
	culture = heunthulyra
    religion = hunt_of_the_myna
	holding = church_holding

    # History
}

2984 = {

    # Misc
	culture = heunthulyra
    religion = hunt_of_the_myna
	holding = none

    # History
}

##d_tousavellen
###c_haqharias
659 = {		#Haqharias

    # Misc
    culture = heunthulyra
    religion = hunt_of_the_eagle
	holding = tribal_holding

    # History
}

2985 = {

    # Misc
	culture = heunthulyra
    religion = hunt_of_the_eagle
	holding = none

    # History
}

2986 = {

    # Misc
	culture = heunthulyra
    religion = hunt_of_the_eagle
	holding = none

    # History
}

###c_jarmevain
658 = {		#Jarmevain

    # Misc
    culture = heunthulyra
    religion = hunt_of_the_eagle
	holding = tribal_holding

    # History
}

2988 = {

    # Misc
	culture = heunthulyra
    religion = hunt_of_the_eagle
	holding = none

    # History
}

2989 = {

    # Misc
	culture = heunthulyra
    religion = hunt_of_the_eagle
	holding = none

    # History
}

2990 = {

    # Misc
	culture = heunthulyra
    religion = hunt_of_the_eagle
	holding = none

    # History
}

###c_setufiraor
662 = {		#Setufiraor

    # Misc
    culture = heunthulyra
    religion = hunt_of_the_eagle
	holding = tribal_holding

    # History
}

2987 = {

    # Misc
	culture = heunthulyra
    religion = hunt_of_the_eagle
	holding = none

    # History
}

###c_hisarytor
657 = {		#Hisarytor

    # Misc
    culture = heunthulyra
    religion = hunt_of_the_eagle
	holding = tribal_holding

    # History
}

2991 = {

    # Misc
	culture = heunthulyra
    religion = hunt_of_the_eagle
	holding = none

    # History
}

###c_tounafira
651 = {		#Tounafira

    # Misc
    culture = heunthulyra
    religion = hunt_of_the_eagle
	holding = tribal_holding

    # History
}

2992 = {

    # Misc
	culture = heunthulyra
    religion = hunt_of_the_eagle
	holding = none

    # History
}

2993 = {

    # Misc
	culture = heunthulyra
    religion = hunt_of_the_eagle
	holding = none

    # History
}

2994 = {

    # Misc
	culture = heunthulyra
    religion = hunt_of_the_eagle
	holding = church_holding

    # History
}

###?
2995 = {

    # Misc
    culture = heunthulyra
    religion = hunt_of_the_eagle
	holding = tribal_holding

    # History
}

2996 = {

    # Misc
	culture = heunthulyra
    religion = hunt_of_the_eagle
	holding = none

    # History
}

##d_Ayarallen
###c_ayarallen
666 = {		#Ayarallen

    # Misc
    culture = heunthulyra
    religion = hunt_of_the_myna
	holding = tribal_holding

    # History
}

2997 = {

    # Misc
	culture = heunthulyra
    religion = hunt_of_the_myna
	holding = church_holding

    # History
}

2998 = {

    # Misc
	culture = heunthulyra
    religion = hunt_of_the_myna
	holding = none

    # History
}

2999 = {

    # Misc
	culture = heunthulyra
    religion = hunt_of_the_myna
	holding = none

    # History
}

###c_arnilqan
667 = {		#Arnilqan

    # Misc
    culture = heunthulyra
    religion = hunt_of_the_myna
	holding = tribal_holding

    # History
}

3000 = {

    # Misc
	culture = heunthulyra
    religion = hunt_of_the_myna
	holding = none

    # History
}

##d_thissilen
###c_thissilen
2885 = {    #Thissilen

    # Misc
    culture = heunthulyra
    religion = hunt_of_the_crane
	holding = tribal_holding

    # History

}
539 = {		#Qasnabor

    # Misc
    culture = heunthulyra
    religion = hunt_of_the_crane
	holding = none

    # History
}

###c_nasru_ean
540 = {		#Nasru-ean

    # Misc
    culture = heunthulyra
    religion = hunt_of_the_crane
	holding = tribal_holding

    # History
}
2886 = {    #Eduz-Arfaya

    # Misc
    culture = heunthulyra
    religion = hunt_of_the_crane
	holding = none

    # History

}
2887 = {    #Nasrusad

    # Misc
    culture = heunthulyra
    religion = hunt_of_the_crane
	holding = none

    # History

}

###c_rianelen
650 = {		#Azkasad

    # Misc
    culture = heunthulyra
    religion = hunt_of_the_myna
	holding = none

    # History
}
2888 = { #Kyrefiraor

    # Misc
    culture = heunthulyra
    religion = hunt_of_the_myna
	holding = none

    # History

}
2889 = { #Rianelen

    # Misc
    culture = heunthulyra
    religion = hunt_of_the_myna
	holding = tribal_holding

    # History

}

###c_daqelum
649 = {		#Daqelum

    # Misc
    culture = heunthulyra
    religion = hunt_of_the_myna
	holding = tribal_holding

    # History
}
2890 = {    #Maermyfira

    # Misc
    culture = heunthulyra
    religion = hunt_of_the_myna
	holding = none

    # History

}