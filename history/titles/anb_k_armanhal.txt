﻿k_armanhal = {
	1000.1.1 = { change_development_level = 10 }
	# 800.1.1 = {
		# succession_laws = { male_preference_law }
	# }
	967.2.21 = {	#whoever Arman's dad is, he dies
		holder = vernid_0003	#Arman Vernid
	}
	989.6.30 = {
		holder = vernid_0005	#Emil, King of Armanhal
	}
}

d_armanhal = {
	967.2.21 = {	#whoever Arman's dad is, he dies
		holder = vernid_0003	#Arman Vernid
	}
	989.6.30 = {
		holder = vernid_0005	#Emil, King of Armanhal
	}
}

c_watchmans_point = {
	970.9.8 = {
		holder = gagliardid_0001 # Roger Gagliardid
	}
	989.6.30 = {
		liege = k_armanhal
	}
}

d_eastneck = {
	1017.5.12 = {	
		liege = k_armanhal
		holder = plainsby_0001	#Ostin Plainsby
	}	
}

c_napesbay = {
	1000.1.1 = { change_development_level = 9 }
	
	989.6.30 = {
		liege = k_armanhal
	}
	991.9.7 = { # Retaking of Napesbay
		holder = gagliardid_0001 # Roger Gagliardid
	}
}

c_stoutharbor = {
	1017.5.12 = {	
		liege = k_armanhal
		holder = plainsby_0001	#Ostin Plainsby
	}	
}

c_heroes_rest = {
	1017.5.12 = {	
		liege = k_armanhal
		holder = plainsby_0001	#Ostin Plainsby
	}	
}
