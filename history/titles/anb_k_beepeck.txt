k_beepeck = {
	1000.1.1 = {
		change_development_level = 8
	}
	1018.4.4 = {
		government = republic_government
		holder = sil_peck_0001 # Ernst síl Peck, former Mayor of Beepeck
	}
	1021.12.1 = {
		holder = sil_bee_0001 # Brayden síl Bee, Mayor of Beepeck
	
	}
}

#d_beepeck = {}

c_beepeck = {
	1000.1.1 = {
		change_development_level = 18
	}
	1018.4.4 = {
		holder = sil_peck_0001 # Ernst síl Peck, former Mayor of Beepeck
	}
	1021.12.1 = {
		holder = sil_bee_0001 # Brayden síl Bee, Mayor of Beepeck
	}
}

b_hiveshold = { #Hiveshold
	1021.12.1 = {
		holder = sil_peck_0002 # Samuel síl Peck, Baron of Hiveshold
	}
}

c_saltmarsh = {
	1000.1.1 = {
		change_development_level = 7
	}
}

c_orston = {
	1022.1.1 = {
		holder = wintersun_0001
		liege = k_beepeck
	}
}

#c_the_strip = {}