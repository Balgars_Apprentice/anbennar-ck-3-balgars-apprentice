﻿
k_deranne = {
	1000.1.1 = { change_development_level = 8 }
	863.5.2 = {
		holder = derhilde_reaverqueen # Derhilde Reaverqueen
	}
	892.11.12 = {
		holder = deranne_0001 # Askel Derhildesson
	}
	912.11.16 = {
		holder = deranne_0003 # Gunnar Derhildesson
	}
	924.5.21 = {
		holder = deranne_0004 # Hjalmar Gunnarsson
	}
	927.8.28 = {
		holder = deranne_0009 # Tomar I of Deranne
	}
	957.8.4 = {
		holder = deranne_0010 # Artus of Deranne
	}
	963.6.26 = {
		holder = deranne_0011 # Tomar II of Deranne
	}
	990.9.20 = {
		holder = deranne_0012 # Kalbard Sil Deranne
	}
}

d_deranne = {
	863.5.2 = {
		holder = derhilde_reaverqueen # Derhilde Reaverqueen
	}
	892.11.12 = {
		holder = deranne_0001 # Askel Derhildesson
	}
	912.11.16 = {
		holder = deranne_0003 # Gunnar Derhildesson
	}
	924.5.21 = {
		holder = deranne_0004 # Hjalmar Gunnarsson
	}
	927.8.28 = {
		holder = deranne_0009 # Tomar I of Deranne
	}
	957.8.4 = {
		holder = deranne_0010 # Artus of Deranne
	}
	963.6.26 = {
		holder = deranne_0011 # Tomar II of Deranne
	}
	990.9.20 = {
		holder = deranne_0012 # Kalbard Sil Deranne
	}
}

c_deranne = {
	1000.1.1 = { change_development_level = 10 }
	
	860.1.6 = {
		holder = derhilde_reaverqueen # Derhilde Reaverqueen
	}
	892.11.12 = {
		holder = deranne_0001 # Askel Derhildesson
	}
	912.11.16 = {
		holder = deranne_0003 # Gunnar Derhildesson
	}
	924.5.21 = {
		holder = deranne_0004 # Hjalmar Gunnarsson
	}
	927.8.28 = {
		holder = deranne_0009 # Tomar I of Deranne
	}
	957.8.4 = {
		holder = deranne_0010 # Artus of Deranne
	}
	963.6.26 = {
		holder = deranne_0011 # Tomar II of Deranne
	}
	990.9.20 = {
		holder = deranne_0012 # Kalbard Sil Deranne
	}
}

c_rosereach = {
	860.1.6 = {
		holder = derhilde_reaverqueen # Derhilde Reaverqueen
	}
	892.11.12 = {
		holder = deranne_0001 # Askel Derhildesson
	}
	912.11.16 = {
		holder = deranne_0003 # Gunnar Derhildesson
	}
	924.5.21 = {
		holder = deranne_0004 # Hjalmar Gunnarsson
	}
	927.8.28 = {
		holder = deranne_0009 # Tomar I of Deranne
	}
	957.8.4 = {
		holder = deranne_0010 # Artus of Deranne
	}
	963.6.26 = {
		holder = deranne_0011 # Tomar II of Deranne
	}
	990.9.20 = {
		holder = deranne_0012 # Kalbard Sil Deranne
	}
}

c_westport = {
	1000.1.1 = { change_development_level = 10 }
}

c_clearshore = {
	1000.1.1 = { change_development_level = 7 }
	
	471.1.1 = { # Iochand
		liege = k_iochand
	}
	622.10.3 = { # War of the Three Roses
		liege = k_enteben
	}
	860.1.6 = {
		holder = derhilde_reaverqueen # Derhilde Reaverqueen
	}
	863.5.2 = {
		liege = k_deranne
	}
	892.11.12 = {
		holder = deranne_0001 # Askel Derhildesson
	}
	912.11.16 = {
		holder = deranne_0003 # Gunnar Derhildesson
	}
	924.5.21 = {
		holder = deranne_0004 # Hjalmar Gunnarsson
	}
	927.8.28 = {
		holder = deranne_0009 # Tomar I of Deranne
	}
	957.8.4 = {
		holder = deranne_0010 # Artus of Deranne
	}
	963.6.26 = {
		holder = deranne_0011 # Tomar II of Deranne
	}
	990.9.20 = {
		holder = deranne_0012 # Kalbard Sil Deranne
	}
}

c_aelvar = {
	800.1.1 = {
		liege = k_deranne
	}
	990.07.02 = {
		holder = 156
	}
	1018.07.15 = {
		holder = 154
	}
}

d_darom = {
	606.4.9 = {
		holder = caylentis_0013 # Alain I Caylentis
	}
	607.7.2 = {
		holder = caylentis_0014 # Adrien I Caylentis
	}
	638.10.7 = {
		holder = caylentis_0015 # Alain II Caylentis
	}
	659.12.29 = {
		holder = caylentis_0016 # Aedan I Caylentis
	}
	690.8.12 = {
		holder = 0
	}
	
	910.4.1 = {
		holder = deranne_0003 # Gunnar Derhildesson
	}
	924.5.21 = {
		holder = deranne_0004 # Hjalmar Gunnarsson
	}
	930.5.26 = {
		holder = deranne_0005 # Haakon Gunnarsson
	}
	956.8.19 = {
		holder = deranne_0009 # Tomar I of Deranne
	}
	957.8.4 = {
		holder = deranne_0010 # Artus of Deranne
	}
	963.6.26 = {
		holder = deranne_0011 # Tomar II of Deranne
	}
	990.9.20 = {
		holder = deranne_0012 # Kalbard Sil Deranne
	}
}

c_hollowview = {
	1000.1.1 = { change_development_level = 7 }

	900.1.1 = {
		liege = k_deranne
	}

	994.1.1 = { # Count Edmond Steedspear
		holder = steedspear_0001
	}

	1010.2.3 = { # Count Artus Steedspear
		holder = steedspear_0002
	}
}

c_lencesk = {
	1000.1.1 = { change_development_level = 11 }
	
	900.1.1 = {
		liege = k_deranne
	}

	973.1.1 = {
		holder = fjorhavn_0001
	}

	995.5.8 = {
		holder = fjorhavn_0002
	}

	1011.1.6 = {
		holder = fjorhavn_0003
	}
}

c_darom = {
	1000.1.1 = { change_development_level = 10 }
	
	910.4.1 = {
		holder = deranne_0003 # Gunnar Derhildesson
	}
	924.5.21 = {
		holder = deranne_0004 # Hjalmar Gunnarsson
	}
	930.5.26 = {
		holder = deranne_0005 # Haakon Gunnarsson
	}
	956.8.19 = {
		liege = k_deranne
		holder = mocard_0001 # Oskar Mocard
	}
	989.10.02 = {
		holder = mocard_0002 # Tomás Mocard
	}
	1001.11.03 = {
		holder = mocard_0003 # Bertran Mocard
	}
}
