k_humacfeld = {
	1000.1.1 = { change_development_level = 8 }
}

d_wallor = {
	960.1.1 = {
		holder = wallor_0008
	}
	1000.1.1 = { change_development_level = 9 }
	1003.5.5 = {
		holder = wallor_0006
	}
	1019.10.10 = {
		holder = 0
	}
}

c_silverdocks = {
	960.1.1 = {
		liege = d_wallor
		holder = wallor_0008
	}
	1000.1.1 = { change_development_level = 13 }
	1003.5.5 = {
		holder = wallor_0006
	}
	1019.10.10 = {
		holder = wallor_0001

	}
}

c_southroad = {
	960.1.1 = {
		liege = d_wallor
		holder = wallor_0008
	}
	1000.1.1 = { change_development_level = 10 }
	1003.5.5 = {
		holder = wallor_0006
	}
	1019.10.10 = {
		holder = wallor_0002
	}
}

c_treomar = {
	975.1.1 = {
		holder = treomar_0005
	}
	1007.1.1 = {
		holder = treomar_0001
	}
}

c_burnoll = {
	1021.8.31 = {
		holder = ancrelor_0001
	}
}

c_smallmere = {
	966.1.1 = {
		holder = vere_0006
	}
	1000.8.1  = {
		holder = vere_0001
	}
}

d_humacvord = {
	955.1.1 = {
		holder = lanwic_0007
	}
	980.1.1 = {
		holder = lanwic_0003
	}
	1014.2.16 = {
		holder = wystanning_0001
	}	
}

c_ionntras = {
	955.1.1 = {
		liege = d_humacvord
		holder = lanwic_0007
	}
	980.1.1 = {
		holder = lanwic_0003
	}
	1000.1.1 = { change_development_level = 11 }
	1014.2.16 = {
		holder = lanwic_0001
	}
}

c_wystanway = {
	947.1.1 = {
		liege = d_humacvord
		holder = wystanning_0007
	}
	992.1.1 = {
		holder = wystanning_0003
	}
	1013.1.1 = {
		holder = wystanning_0001
	}
	1000.1.1 = { change_development_level = 9 }	
}

c_forkwic = {
	977.1.1 = {
		liege = d_humacvord
		holder = forkwic_0001
	}
	1000.1.1 = { change_development_level = 9 }
}

c_grannvale = {
	1017.1.1 = {
		liege = 0
		holder = grannvale_0001
	}	
}

c_humacs_rest = {
	956.1.1 = {
		liege = 0
		holder = humacsael_0007
	}
	980.1.1 = {
		holder = humacsael_0006
	}
	1010.3.14 = {
		holder = humacsael_0001
	}
}

c_camircost = {
	983.1.1 = {
		holder = meregard_0005
	}
	1000.1.1 = { change_development_level = 10 }
	1017.6.15 = {
		holder = meregard_0001
	}
}

c_haresleigh = {
	986.1.1	 = {
		holder = humacsael_0001
	}
}

c_theminath = {
	1020.8.16 = {
		holder = meregard_0001
	}	
}